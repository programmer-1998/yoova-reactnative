import {Text, View} from "react-native";
import {StatusBar} from "expo-status-bar";

import styles from "./AppStyle";
function AppUX(){
    return(
        <View style={styles.container}>
            <Text>Test is done!</Text>
            <StatusBar style="auto" />
        </View>
    );
}

export default AppUX;